using UnityEngine;

namespace Behaviour
{
    public class AutoTarget : MonoBehaviour
    {
        public Transform body;
        public float minimumDotRequired = 0.5F;

        private GrabPoint[] _grabPoints;

        private void Start()
        {
            _grabPoints = FindObjectsOfType<GrabPoint>();
        }

        private void Update()
        {
            ScanActiveGrabPoint();
        }

        private void ScanActiveGrabPoint()
        {
            if (_grabPoints.Length == 0)
                return;

            var forward3 = this.body.forward;
            var forward = new Vector2(forward3.x, forward3.z).normalized;

            GrabPoint closest = null;
            var bestDot = this.minimumDotRequired;
            foreach (var grabPoint in _grabPoints)
            {
                var pos = grabPoint.transform.position;
                var flattened = new Vector2(pos.x, pos.z);
                var dot = Vector2.Dot(forward, flattened.normalized);

                if (dot <= bestDot)
                    continue;

                closest = grabPoint;
                bestDot = dot;
            }

            if (closest != _activeGrabPoint)
                ActiveGrabPoint = closest;
        }

        private GrabPoint _activeGrabPoint;

        private GrabPoint ActiveGrabPoint
        {
            set
            {
                if (_activeGrabPoint)
                    _activeGrabPoint.Targeted = false;

                _activeGrabPoint = value;

                if (_activeGrabPoint)
                    _activeGrabPoint.Targeted = true;
            }
        }

        public Vector3 CurrentTarget 
            => (_activeGrabPoint)
                ? _activeGrabPoint.transform.position
                : Vector3.zero;
    }
}
