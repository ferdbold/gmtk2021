using UnityEngine;

namespace Behaviour
{
    public class CameraMovement : MonoBehaviour
    {
        public Transform target;

        [Range(0F, 10F)] public float cameraSpeed = 0.78F;
        public float deadzone = 4F;

        private Vector3 _targetPosition;

        private void Update()
        {
            UpdateTargetPosition();
        }

        private void LateUpdate()
        {
            MoveCamera();
        }

        private void UpdateTargetPosition()
        {
            var pos = this.target.position;
            pos.x = 0.0F;
            _targetPosition = pos;
        }

        private void MoveCamera()
        {
            var pos = transform.position;
            var distance = Vector3.Distance(pos, _targetPosition);
            if (distance < this.deadzone)
                return;

            transform.position = 
                (pos * (1 - this.cameraSpeed / 100)) +
                (_targetPosition * this.cameraSpeed / 100);
        }
    }
}
