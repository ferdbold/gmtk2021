using UnityEngine;
using UnityEngine.InputSystem;

namespace Behaviour
{
    public class StickyHand : MonoBehaviour
    {
        public MeshRenderer[] armRenderers;
        public float forceStrength = 30000F;

        public float swingDrag = 0.2F;
        public float swingDamper = 20F;

        private Rigidbody _body;
        private SpringJoint _armJoint;
        private FixedJoint _stickJoint;
        private AutoTarget _autoTarget;

        private float _defaultDrag;
        private float _defaultDamper;

        private void Awake()
        {
            _body = GetComponent<Rigidbody>();
            _armJoint = GetComponent<SpringJoint>();
            _autoTarget = GameObject.FindWithTag("Player").GetComponent<AutoTarget>();

            _defaultDrag = _body.drag;
            _defaultDamper = _armJoint.damper;
        }

        public void OnSwing(InputAction.CallbackContext context)
        {
            if (!context.performed)
                return;

            if (!_autoTarget)
                return;

            _body.drag = this.swingDrag;
            _armJoint.damper = this.swingDamper;
            
            _body.AddForce((_autoTarget.CurrentTarget - transform.position) * this.forceStrength);
            
            SetLayerSticky(true);
            SetShaderIsSwinging(true);
        }

        public void OnRecall(InputAction.CallbackContext context)
        {
            if (!context.performed)
                return;

            _body.drag = _defaultDrag;
            _armJoint.damper = _defaultDamper;
            
            UnstickFromGrabPoint();
            SetLayerSticky(false);
            SetShaderIsSwinging(false);
        }

        private void OnTriggerEnter(Collider other)
        {
            if (other.gameObject.layer == LayerMask.NameToLayer("Grab Point"))
                StickToGrabPoint(other);
        }

        private void StickToGrabPoint(Collider grabPoint)
        {
            _stickJoint = gameObject.AddComponent<FixedJoint>();
            _stickJoint.connectedBody = grabPoint.attachedRigidbody;
        }

        private void UnstickFromGrabPoint()
        {
            if (_stickJoint == null)
                return;
            
            _stickJoint.connectedBody = null;
            Destroy(_stickJoint);
        }

        private void SetLayerSticky(bool value)
        {
            gameObject.layer = LayerMask.NameToLayer((value) ? "Sticky" : "Default");
        }
        
        private void SetShaderIsSwinging(bool value)
        {
            foreach (var armRenderer in this.armRenderers)
                armRenderer.material.SetFloat(Shader.PropertyToID("IsSwinging"), (value) ? 1F : 0F);
        }
    }
}
