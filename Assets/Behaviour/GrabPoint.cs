using UnityEngine;

namespace Behaviour
{
    public class GrabPoint : MonoBehaviour
    {
        public GameObject spriteGameObject;

        private Animator _animator;
        private int _targetedPropID;

        private void Awake()
        {
            _animator = this.spriteGameObject.GetComponent<Animator>();
            _targetedPropID = Animator.StringToHash("Targeted");
        }

        public bool Targeted
        {
            set => _animator.SetBool(_targetedPropID, value);
        }
    }
}
