using UnityEngine;
using UnityEngine.InputSystem;

namespace Behaviour
{
    public class CharacterMovement : MonoBehaviour
    {
        public CharacterController characterController;
        public Transform body;
        public float moveSpeed = 70F;

        private Vector2 _velocity;

        private void Update()
        {
            ApplyMove();
        }

        public void OnMove(InputAction.CallbackContext context)
        {
            _velocity = context.ReadValue<Vector2>();
        }

        private void ApplyMove()
        {
            // Translate character
            var move = new Vector3(_velocity.x, 0F, _velocity.y) * this.moveSpeed;
            this.characterController.Move((move + Physics.gravity) * Time.deltaTime);

            // Face character forward
            if (_velocity.magnitude <= 0.2F)
                return;

            var angle = -Vector2.SignedAngle(Vector2.up, _velocity);
            var eulerAngles = this.body.localEulerAngles;
            this.body.localEulerAngles = new Vector3(eulerAngles.x, angle, eulerAngles.z);
        }
    }
}
